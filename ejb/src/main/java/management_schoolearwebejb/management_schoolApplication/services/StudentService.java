package management_schoolearwebejb.management_schoolApplication.services;



import jakarta.ejb.Stateful;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import management_schoolearwebejb.management_schoolApplication.entities.Student;

@Stateful
public class StudentService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public Student saveStudent(Student student) {
		entityManager.persist(student);	
		return student;
	}

}
