package management_schoolearwebejb.management_schoolApplication.services;

import java.util.List;
import java.util.Optional;

import jakarta.ejb.Stateful;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import management_schoolearwebejb.management_schoolApplication.entities.Classroom;

@Stateful
public class ClassroomService {
	
	@PersistenceContext
	public EntityManager entityManager;
	
	public Classroom addClassroom(Classroom classroom) {
		 entityManager.persist(classroom);
		 return classroom;
	}
	
	public List<Classroom>findAllClassrooms(){
		return entityManager.createQuery("SELECT * FROM Classroom").getResultList();
	}
	
	public List<Classroom>findClassroomByName(String name){
		return entityManager.createNamedQuery("findClassroomByName")
				.setParameter("name", name)
				.getResultList();
	}
	public Classroom findClassroomById(long id) {
		return (Classroom) entityManager.createNativeQuery("findClassroomById")
				.setParameter("id", id)
				.getSingleResult();
	}
	
	

}
